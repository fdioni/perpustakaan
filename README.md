# Perpustakaan

### Buatlah program web Perpustakaan sederhana dengan spesifikasi dan fungsi sebagai berikut.

1.	Fungsi dasar
    1.	CRUD
    2.	Autentikasi
    3.	API RESTful JSON
2.	Fitur Admin
    1.	Login 
    2.	User Management (CRUD)
3.	Search Buku by ISBN API: https://openlibrary.org/dev/docs/api/books
4.	List Buku (CRUD) [id, title, isbn, description]
    - Tambahkan Buku Ke Database
    - Edit Data Buku
    - Hapus Buku
5.	List Pinjam (R) [id,user_id,buku_id]
6.	Detail User (R)
7.	Profile Admin
3.	Fitur User
    1.	Login Page & Dashboard
    2.	Profile
    3.	Daftar Buku (RU)
        - Tambahkan buku
        - Kembalikan buku
